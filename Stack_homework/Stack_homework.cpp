﻿#include <iostream>
using namespace std;

class Stack
{
public:
	//главная исполнительная функция,вызывающая Array()
	void Func()
	{
		int value;
		cout << "Enter any integer value: ";
		cin >> value;
		cout << endl;
		Array(value);
	}

	//Функция обрабатывает массивы стэка
	void Array(int size)
	{
		size;//(стандартное значение,введенное пользователем)
		int* arr = new int[size] {};//создание динамического массива

		for (int i = 0; i < size; i++)
		{
			arr[i] = i;// индексация массива 
			cout << arr[i] << '\t'; //вывод проиндексированного массива
		}

		int UpStack = arr[size - 1];//верхний элемент стека
		int AddStack = size + 1;//добавляет элемент стэка 

		cout << endl << "Up stack element: " << UpStack << endl << endl << endl; // вывод верхннего элемента стека	

		//Вызов метода pop()
		pop(UpStack);
		cout << endl << "Stack without up element " << endl << endl;

		//Вызов метода push() 
		push(AddStack);
		cout << endl << "Added stack element: " << AddStack - 1 << endl;
		delete[] arr; //сборка
		arr = nullptr;//мусора
	}

	//метод pop(), чтобы достать из стека верхний элемент
	void pop(int size)
	{
		size;//(значение,введенное пользователем) - 1
		int* arr = new int[size] {};

		for (int i = 0; i < size; i++)
		{
			arr[i] = i;
			cout << arr[i] << '\t';
		}
		delete[] arr;
		arr = nullptr;
	}

	//метод push(),чтобы добавить новый элемент
	void push(int size)
	{
		size;//(значение,введенное пользователем) + 1
		int* arr = new int[size] {};

		for (int i = 0; i < size; i++)
		{
			arr[i] = i;// индексация массива 
			cout << arr[i] << '\t'; //вывод проиндексированного массива
		}
		delete[] arr;
		arr = nullptr;
	}
};

int main()
{
	Stack a;
	a.Func();

	////альтернативный вызов
	//Stack* p = new Stack;
	//p->Func();
	//delete p;
	//p = nullptr;

	return 0;
}